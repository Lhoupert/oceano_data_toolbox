function [dcl,dof] = decorrelation3(x,y,diag) ;
%% Compute auto-correlation of a variable; determine the weighted integral scale; 
% compute degrees of freedom in data series
% Compute the integral time scale and degrees of freedom in a timeseries
% Emery, W. J. and R. E. Thomson, 1997: Data analysis methods in physical
% oceanography. Pergamon, 634 pp. (see p263,265,379-380) define the
% integral time scale from C(0) out to the first zero crossing. Here we
% define the integral time scale as twice this value.
% Integral time scale = 2xsum(C(tau)dt) from C(0) to first zero crossing.
% If the autocorrelation decays linearly then the area under this triangle
% is 0.5 * distance to zero crossing. So twice this integral is equal to the 
% time to the zero crossing.
% If the correlation decays fast initially but more slowly later the zero
% crossing suggests a longer time than the sum which is really just a
% weighted estimate and in this case gives less weight to a long tail with
% low correlation.
%
%
% USAGE : function [dcl,dof] = decorrelation3(x,y,diag)
% x is the temporal/spatial variable ; 
% y is the variable and is normalised prior to computing the autocorrelation
% Diagnostic diag = 1/0 for diagnostic plots.
% dcl is the decorrelation length scale in the units of x
% dof is the number of degrees of freedom in x. Calculated by length(x) /
% dcl.
% Stuart Cunningham, July 2017


doplot = diag;

x(isnan(y))=[];
y(isnan(y))=[];
% First normalise the variable
ynorm = (y - nanmean(y)) / nanstd(y);

maxlag = length(ynorm); % Set maxlag = length of the variable

[C,lags] = xcorr(ynorm,maxlag,'coeff'); % compute normalised correlation coefficient
[X0 Y0] = intersections(lags,C,[min(lags),max(lags)],[0.0 0.0]); % use functions to find the intersection of correlation with line y=0;


X0lag = (X0(((length(X0)/2)+1))); % find lag value of first negative crossing
[~,Imax]=min(abs(lags - X0lag)) ; % find index of first negative crossing
[~,Imin]=min(abs(lags - -X0lag));% find index of first positive crossing

dcl = trapz(lags(Imin:Imax),C(Imin:Imax)); % Integrate correlation between first neg and first pos crossing. This is the decorrelation length
dof= length(ynorm) ./ dcl ; % Degrees of freedom = length of y / dcl

disp(['x has',sprintf('%5.0f',length(x)),' data cycles']);
disp(['Integral time scale = ',sprintf('%2.1f',dcl) ' * unit of time',...
    ' : Degrees of freedom = ',sprintf('%2.0f',dof)])

if doplot;
    figure;clf;
    subplot(211);hold on ;grid on;
    plot(x,ynorm,'k');
    title(['x has ',sprintf('%5.1f',length(x)),' data cycles']);
    xlabel('x variable');ylabel('Normalised y variable');
    
    subplot(212);hold on;grid on;
    plot(lags,C);
    plot(X0,Y0,'ko','MarkerSize',10,'MarkerFaceColor','k','MarkerSize',5);
    xlim([lags(Imin-20) lags(Imax+20)]);
    xlabel('Lags');title('Normalised auto-correlation of ynorm');ylabel('Correlation coefficient');
end


end
